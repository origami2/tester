var NodeVM = require('vm2').NodeVM;

var Aggregator = require('./aggregator');


function MochaLoader() {
}

MochaLoader.prototype.loadFile = function (path) {
  if (!path) throw new Error('path is required');
  
  return new Promise(function (resolve, reject) {
    try {
      var aggregator = new Aggregator(path);

      var vm = new NodeVM({
        console: 'inherit',
        sandbox: {
          describe: aggregator.makeDescribe(),
          it: aggregator.makeIt(),
          before: aggregator.makeBefore(),
          after: aggregator.makeAfter(),
          beforeEach: aggregator.makeBeforeEach(),
          afterEach: aggregator.makeAfterEach(),
          context: aggregator.makeContext()
        },
        require: {
            external: true,
            builtin: [
              'fs',
              'path',
              'assert',
              'events'
            ]
        }
      });
      
      vm.run(require('fs').readFileSync(path, 'utf8'), path);

      resolve(aggregator.getRootSuite());
    } catch (e) {
      reject(e);
    }
  });
};

module.exports = MochaLoader;