var Suite = require('../suite');
var Test = require('../test');
var Runner = require('../runner');
var assert = require('assert');

describe('Suite', function () {
  it('requires title', function () {
    assert.throws(
      function () {
        new Suite();
      },
      /title is required/
    );
  });
  
  describe('#.getAllTests', function () {
    it('returns nested tests', function () {
      var r = new Suite('r');
      var s2 = new Suite('s2', r);
      r.addSuite(s2);
      
      var s3 = new Suite('s3', r);
      r.addSuite(s3);
      
      var s4 = new Suite('s4', s2);
      s2.addSuite(s4);
      
      r.addTest(new Test('t1', function () {}));
      s2.addTest(new Test('t2', function () {}));
      s3.addTest(new Test('t3', function () {}));
      s4.addTest(new Test('t4', function () {}));
      
      var all = r.getAllTests();
      var names = all.map(function (t) { return t.getTitle(); });
      
      assert.deepEqual(names, ['t4', 't2', 't3', 't1']);
    });
  });
  
  
  
  describe('#.getTitle', function () {
    it('returns the title', function () {
      var suite = new Suite('some title');
      
      assert.equal(suite.getTitle(), 'some title');
    });
  });
  
  describe('#.addTest', function () {
    it('requires test', function () {
      assert.throws(
        function () {
          var target = new Suite('title');
        
          target.addTest();
        },
        /test is required/
      );
    });
  });
  
  describe('.addSuite', function () {
    it('requires suite', function () {
      assert.throws(
        function () {
          var target = new Suite('title');
        
          target.addSuite();
        },
        /suite is required/
      );
    });
    
    it('accepts suite', function () {
      var target = new Suite('title');
    
      target.addSuite({});
    });
    
    it('do not adds suite twice', function () {
      var target = new Suite('title');
    
      var child = {};
      
      target.addSuite(child);
      target.addSuite(child);
      
      assert.equal(1, target.suites.length);
    });
  });
  
  describe('#.getSuites', function () {
    it('returns array', function () {
      var target = new Suite('title');
    
      assert(target.getSuites() instanceof Array);
    });
  });
  
  describe('#.getTests', function () {
    it('returns array', function () {
      var target = new Suite('title');
    
      assert(target.getTests() instanceof Array);
    });
    
    it('includes tests', function () {
      var target = new Suite('title');
      
      target.addTest(new Test('test 1', function () {}));
    
      assert.equal(target.getTests().length, 1);
    });
  });
  
  describe('.run', function () {
    it('requires runner', function () {
      assert.throws(
        function () {
          var target = new Suite('title');
        
          target.run();
        },
        /runner is required/
      );
    });
    
    it('runs tests', function (done) {
      var target = new Suite('title');
      var ran = false;
      
      var test1 = new Test('test 1', 
      function () {
        ran = true;
      });
    
      target
      .addTest(
        test1
      );
      
      var runner = new Runner();
      
      target
      .run(runner)
      .then(function () {
        try {
          assert(ran);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
    
    it('runs suite', function (done) {
      var target = new Suite('title');
      var ran = false;
    
      target
      .addSuite(
        {
          getTitle: function () { return 'subsuite'; },
          run: function () { ran = true; return Promise.resolve(); }
        }
      );
      
      var runner = new Runner();
      
      target
      .run(runner)
      .then(function () {
        try {
          assert(ran);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
  });
  
  describe('#.getBeforeHooks', function () {
    it('returns array', function () {
      var target = new Suite('my suite');
      
      assert(target.getBeforeHooks() instanceof Array);
    });
  });
  
  describe('#.addBefore', function () {
    it('accepts function', function () {
      var target = new Suite('my suite');
    
      target.addBefore(function () {});
    });
    
    it('lists it', function () {
      var target = new Suite('my suite');
    
      target.addBefore(function () {});
    
      var beforeHooks = target.getBeforeHooks();
      
      assert.equal(beforeHooks.length, 1);
    });
    
    it('runs it before test', function (done) {
      var ran = [];
      var target = new Suite('my suite');
      
      target.addBefore(function () {
        ran.push('before');
      });
    
      target.addTest(new Test('my test', function () {
        ran.push('test');
      }));
      var runner = new Runner();
      
      target
      .run(runner)
      .then(function () {
        try {
          assert.deepEqual(ran, ['before', 'test']);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
  });
  
  describe('#.addBeforeEach', function () {
    it('accepts function', function () {
      var target = new Suite('my suite');
    
      target.addBeforeEach(function () {});
    });
    
    it('lists it', function () {
      var target = new Suite('my suite');
    
      target.addBeforeEach(function () {});
    
      var beforeEachHooks = target.getBeforeEachHooks();
      
      assert.equal(beforeEachHooks.length, 1);
    });
    
    it('runs it before each test', function (done) {
      var ran = [];
      var target = new Suite('my suite');
      
      target
      .addBeforeEach(
        function () {
          ran.push('before ' + String(ran.length));
        }
      );
    
      target
      .addTest(
        new Test(
          'test-1',
          function () {
            ran.push('test-1');
          }
        )
      );
    
      target
      .addTest(
        new Test(
          'test-2',
          function () {
            ran.push('test-2');
          }
        )
      );
      
      var runner = new Runner();
      
      target
      .run(runner)
      .then(function () {
        try {
          assert.deepEqual(ran, ['before 0', 'test-1', 'before 2', 'test-2']);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
  });
  
  describe('#.addAfterEach', function () {
    it('accepts function', function () {
      var target = new Suite('my suite');
    
      target.addAfterEach(function () {});
    });
    
    it('lists it', function () {
      var target = new Suite('my suite');
    
      target.addAfterEach(function () {});
    
      var afterEachHooks = target.getAfterEachHooks();
      
      assert.equal(afterEachHooks.length, 1);
    });
    
    it('runs it after each test', function (done) {
      var ran = [];
      var target = new Suite('my suite');
      
      target
      .addAfterEach(
        function () {
          ran.push('after-each-' + String(ran.length));
        }
      );
    
      target
      .addTest(
        new Test(
          'test-1',
          function () {
            ran.push('test-1');
          }
        )
      );
    
      target
      .addTest(
        new Test(
          'test-2',
          function () {
            ran.push('test-2');
          }
        )
      );
      
      var runner = new Runner();
      
      target
      .run(runner)
      .then(function () {
        try {
          assert.deepEqual(ran, ['test-1', 'after-each-1', 'test-2', 'after-each-3']);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
  });
  
  describe('#.getAfterHooks', function () {
    it('returns array', function () {
      var target = new Suite('my suite');
      
      assert(target.getAfterHooks() instanceof Array);
    });
  });
  
  describe('#.addAfter', function () {
    it('accepts function', function () {
      var target = new Suite('my suite');
    
      target.addAfter(function () {});
    });
    
    it('lists it', function () {
      var target = new Suite('my suite');
    
      target.addAfter(function () {});
    
      var afterHooks = target.getAfterHooks();
      
      assert.equal(afterHooks.length, 1);
    });
    
    it('runs it after test', function (done) {
      var ran = [];
      var target = new Suite('my suite');
      
      target.addAfter(function () {
        ran.push('after');
      });
    
      target.addTest(new Test('my test', function () {
        ran.push('test');
      }));
      var runner = new Runner();
      
      target
      .run(runner)
      .then(function () {
        try {
          assert.deepEqual(ran, ['test', 'after']);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
  });
});