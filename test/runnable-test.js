describe('Runnable', function () {
  var Runnable = require('../runnable');
  var assert = require('assert');
  
  it('requires implementation', function () {
    assert.throws(
      function () {
        new Runnable();
      },
      /implementation is required/
    );
  });
  
  it('accepts implementation', function () {
    new Runnable(function () {});
  });
  
  describe('.run',function () {
    it('runs implementation with no arguments', function (done) {
      var target = new Runnable(function () {
        done();
      });
      
      target
      .run();
    });
    
    it('captures error', function (done) {
      var target = new Runnable(function () {
        throw 'my error';
      });
      
      target
      .run()
      .catch(function (err) {
        try {
          assert.equal(err, 'my error');
          
          done();
        } catch (e) {
          done(e);
        }
      })
    });
    
    it('runs implementation with done argument', function (done) {
      var target = new Runnable(function (testDone) {
        try {
          assert(testDone);
          assert.equal(typeof(testDone), 'function');
          
          done();
        } catch (e) {
          done(e);
        }
      });
      
      target
      .run();
    });
    
    it('captures error with done argument', function (done) {
      var target = new Runnable(function (testDone) {
        testDone('my error');
      });
      
      target
      .run()
      .catch(function (err) {
        try {
          assert.equal(err, 'my error');
          
          done();
        } catch (e) {
          done(e);
        }
      })
    });
  });
});