describe('Aggregator', function () {
  var Aggregator = require('../aggregator');
  var Suite = require('../suite');
  var assert = require('assert');
  
  describe('#constructor', function () {
    it('requires a filename', function () {
      assert.throws(
        function () {
          new Aggregator();
        },
        /filename is required/
      );
    });
    
    it('accepts a filename', function () {
      new Aggregator('my-file.js');
    });
  });
  
  describe('#.getTests', function () {
    it('returns an array', function () {
      var target= new Aggregator('my-file.js');
      
      assert(target.getTests() instanceof Array);
    });
  });
  
  describe('#.getRootSuite', function () {
    it('returns a suite', function () {
      var target = new Aggregator('my-file.js');
      
      var rootSuite = target.getRootSuite();
      
      assert(rootSuite);
      assert(rootSuite instanceof Suite);
      assert.equal(rootSuite.getTitle(), 'my-file.js');
    });
  });
  
  describe('creating an instance', function () {
    var target;
    
    it('accepts a file name', function () {
      target = new Aggregator('some-file.js');
    });
    
    describe('#.makeBefore', function () {
      var beforeFn;
      
      before(function () {
        beforeFn = target.makeBefore();
      });
      
      it('returns a function', function () {
        assert(typeof(beforeFn), 'function');
      });
      
      it('adds a before function', function () {
        beforeFn(function () {});
        
        assert.equal(target.getRootSuite().getBeforeHooks().length, 1);
      });
    });
    
    describe('#.makeBeforeEach', function () {
      var beforeEachFn;
      
      before(function () {
        beforeEachFn = target.makeBeforeEach();
      });
      
      it('returns a function', function () {
        assert(typeof(beforeEachFn), 'function');
      });
      
      it('adds a before function', function () {
        beforeEachFn(function () {});
        
        assert.equal(target.getRootSuite().getBeforeEachHooks().length, 1);
      });
    });
    
    describe('#.makeAfter', function () {
      var afterFn;
      
      before(function () {
        afterFn = target.makeAfter();
      });
      
      it('returns a function', function () {
        assert(typeof(afterFn), 'function');
      });
      
      it('adds an after function', function () {
        afterFn(function () {});
        
        assert.equal(target.getRootSuite().getAfterHooks().length, 1);
      });
    });
    
    describe('#.makeAfterEach', function () {
      var afterEachFn;
      
      before(function () {
        afterEachFn = target.makeAfterEach();
      });
      
      it('returns a function', function () {
        assert(typeof(afterEachFn), 'function');
      });
      
      it('adds a before function', function () {
        afterEachFn(function () {});
        
        assert.equal(target.getRootSuite().getAfterEachHooks().length, 1);
      });
    });
    
    describe('#.makeDescribe', function () {
      var describeFn;
      
      before(function () {
        describeFn = target.makeDescribe();
      });
      
      it('returns a describe function', function () {
        assert.equal(typeof(describeFn), 'function');
      });
      
      it('invoking it adds a suite', function () {
        describeFn('my sub-suite 1', function () {});
        
        var subSuite = target.getRootSuite().getSuites()[0];
        
        assert(subSuite);
        assert.equal(subSuite.getTitle(), 'my sub-suite 1');
      });
      
      it('invoking it adds another suite', function () {
        describeFn('my sub-suite 2', function () {});
        
        var subSuite = target.getRootSuite().getSuites()[1];
        
        assert(subSuite);
        assert.equal(subSuite.getTitle(), 'my sub-suite 2');
      });
      
      describe('#makeIt', function () {
        var itFn;
        
        before(function () {
          itFn = target.makeIt();
        });
        
        it('returns an it function', function () {
          assert.equal(typeof(itFn), 'function');
        });
        
        it('adds a test at root level', function () {
          itFn('test-1', function () {});
          
          assert.equal(target.getTests().length, 1);
        });
        
        context('#within describe', function () {
          it('adds a test at sub-suite level', function () {
            describeFn('my-subsuite-3', function () {
              itFn('my-test-2', function () {
              });
            });
            
            var lastLevel2suite = target.getRootSuite().getSuites().slice(-1)[0];
            
            assert(lastLevel2suite);
            assert.equal(lastLevel2suite.getTitle(), 'my-subsuite-3');
            
           var lastTest = target.getTests().slice(-1)[0];
            assert.equal(lastTest.getTitle(), 'my-test-2');
            assert.equal(lastTest, lastLevel2suite.getTests().slice(-1)[0]);
          });
        });
    
        describe('#.makeContext', function () {
          var contextFn;
          
          before(function () {
            contextFn = target.makeContext();
          });
          
          it('returns a function', function () {
            assert(typeof(contextFn), 'function');
          });
        });
      });
    });
  });
});