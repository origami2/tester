var ObjectReporter = require('../object-reporter');
var assert = require('assert');
var EventEmitter = require('events').EventEmitter;

describe('ObjectReporter', function () {
  it('requires a runner', function () {
    assert.throws(
      function () {
        new ObjectReporter();
      },
      /runner is required/
    );
  });
  
  describe('#.getResults', function () {
    var runner;
    var target;
    
    beforeEach(function () {
      runner = new EventEmitter();
      target = new ObjectReporter(runner);
    });
   
    it('returns empty results at first', function () {
      assert.deepEqual(target.getResults(), {});
    });
   
    it('returns suite started', function () {
      var startDate = Date.now();
      
      runner
      .emit(
        'start-run',
        {
          kind: 'suite',
          title: 's1',
          date: startDate
        }
      );
      
      assert
      .deepEqual(
        target.getResults(), 
        {
          's1': {
            kind: 'suite',
            start: startDate
          }
        }
      );
    });
   
    it('returns suite stopped', function () {
      var startDate = Date.now();
      
      runner
      .emit(
        'start-run',
        {
          kind: 'suite',
          title: 's1',
          date: startDate
        }
      );
      
      var endDate = startDate + 100;
      
      runner
      .emit(
        'stop-run',
        {
          kind: 'suite',
          title: 's1',
          date: endDate
        }
      );
      
      assert
      .deepEqual(
        target.getResults(), 
        {
          's1': {
            kind: 'suite',
            start: startDate,
            end: endDate,
            took: 100
          }
        }
      );
    });
   
    it('returns suite stopped', function () {
      var startDate = Date.now();
      
      runner
      .emit(
        'start-run',
        {
          kind: 'suite',
          title: 's1',
          date: startDate
        }
      );
      
      var endDate = startDate + 100;
      
      runner
      .emit(
        'stop-run',
        {
          kind: 'suite',
          title: 's1',
          date: endDate
        }
      );
      
      assert
      .deepEqual(
        target.getResults(), 
        {
          's1': {
            kind: 'suite',
            start: startDate,
            end: endDate,
            took: 100
          }
        }
      );
    });
  });
});