describe('d5', function () {
  var v;
  before(function () {
    v = 1;
  });
  
  it('t5.1', function () {
    require('assert').equal(v, 1);
  });
  
  it('t5.2', function () {
    require('assert').equal(v, 1);
    v = 2;
  });
  
  after(function () {
    require('assert').equal(v, 2);
  });
});