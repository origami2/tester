describe('d6', function () {
  var v = [];
  
  beforeEach(function () {
    v.push(v.length);
  });
  
  afterEach(function () {
    v.push(v.length);
  });
  
  it('t6.1', function () {
    require('assert').deepEqual(v, [0]);
  });
  
  it('t6.2', function () {
    require('assert').deepEqual(v, [0, 1, 2]);
  });
});