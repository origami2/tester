var assert = require('assert');
var MochaLoader = require('../mocha-loader');
var Runner = require('../runner');

describe('MochaLoader', function() {
  describe('.loadTest', function() {
    var target;

    beforeEach(function() {
      target = new MochaLoader();
    });

    it('requires a path', function() {
      assert.throws(
        function() {
          target.loadFile();
        },
        /path is required/
      );
    });

    it('loads test', function(done) {
      target
        .loadFile(
          require('path').join(__dirname, 'sample-tests', 'test1.js'))
        .catch(done)
        .then(function(suite) {
          try {
            assert(suite);
            
            done();
          }
          catch (e) {
            done(e);
          }
        });
    });

    it('runs test', function(done) {
      var runner = new Runner();

      var ran = [];

      runner
        .on('start-run', function(metadata) {
          ran.push(metadata);
        });

      var testPath = require('path').join(__dirname, 'sample-tests', 'test1.js');

      target
        .loadFile(testPath)
        .catch(done)
        .then(function(suite) {
          suite
            .run(runner)
            .then(function() {
              try {
                assert.deepEqual(
                  ran, [{
                    kind: 'suite',
                    title: 'd1',
                    path: [testPath, 'd1']
                  },{
                    kind: 'test',
                    title: 't1',
                    path: [testPath, 'd1', 't1']
                  }]);

                done();
              }
              catch (e) {
                done(e);
              }
            });
        });
    });

    it('runs failing tests', function(done) {
      var runner = new Runner();

      runner
        .on('run-fail', function(metadata) {
          try {
            assert(metadata.error);

            done();
          }
          catch (e) {
            done(e);
          }
        });

      target
        .loadFile(require('path').join(__dirname, 'sample-tests', 'test2.js'))
        .catch(done)
        .then(function(suite) {
          suite
            .run(runner);
        });
    });

    it('runs both sync and async tests', function(done) {
      var runner = new Runner();

      var ran = [];

      runner
        .on('run-ok', function(metadata) {
          if (metadata.kind !== 'test') return;
          ran.push(['ok', metadata]);
        });

      runner
        .on('run-fail', function(metadata) {
          if (metadata.kind !== 'test') return;
          ran.push(['fail', metadata]);
        });

      target
        .loadFile(require('path').join(__dirname, 'sample-tests', 'test3.js'))
        .catch(done)
        .then(function(suite) {
          suite
            .run(runner)
            .then(function() {
              try {
                assert.equal(ran.length, 4);
                assert.equal(ran[0][0], 'ok');
                assert.equal(ran[1][0], 'fail');
                assert.equal(ran[2][0], 'ok');
                assert.equal(ran[3][0], 'fail');
                assert.equal(ran[0][1].title, 't3');
                assert.equal(ran[1][1].title, 't4');
                assert.equal(ran[2][1].title, 't5');
                assert.equal(ran[3][1].title, 't6');
                assert(ran[1][1].error);
                assert(ran[1][1].error instanceof Error);
                assert.equal(ran[1][1].error.message, 'oops');
                assert(ran[3][1].error);
                assert.equal(ran[3][1].error, 'oops');

                done();
              }
              catch (e) {
                done(e);
              }
            });
        });
    });
    
    it('runs test with before and after', function(done) {
      var runner = new Runner();

      var ran = [];

      runner
        .on('start-run', function(metadata) {
          ran.push(metadata);
        });

      var testPath = require('path').join(__dirname, 'sample-tests', 'test4.js');

      target
        .loadFile(testPath)
        .catch(done)
        .then(function(suite) {
          suite
            .run(runner)
            .then(function() {
              try {
                assert.deepEqual(
                  ran, [{
                    kind: 'suite',
                    title: 'd5',
                    path: [testPath, 'd5']
                  },{
                    kind: 'before',
                    title: 'd5',
                    path: [testPath, 'd5']
                  },{
                    kind: 'test',
                    title: 't5.1',
                    path: [testPath, 'd5', 't5.1']
                  },{
                    kind: 'test',
                    title: 't5.2',
                    path: [testPath, 'd5', 't5.2']
                  },{
                    kind: 'after',
                    title: 'd5',
                    path: [testPath, 'd5']
                  }]);

                done();
              }
              catch (e) {
                done(e);
              }
            });
        });
    });
    
    it('runs test with before and after', function(done) {
      var runner = new Runner();

      var ran = [];

      runner
        .on('start-run', function(metadata) {
          ran.push(metadata);
        });

      var testPath = require('path').join(__dirname, 'sample-tests', 'test5.js');

      target
        .loadFile(testPath)
        .catch(done)
        .then(function(suite) {
          suite
            .run(runner)
            .then(function() {
              try {
                assert.deepEqual(
                  ran, [{
                    kind: 'suite',
                    title: 'd6',
                    path: [testPath, 'd6']
                  },{
                    kind: 'before-each',
                    title: 'd6',
                    path: [testPath, 'd6']
                  },{
                    kind: 'test',
                    title: 't6.1',
                    path: [testPath, 'd6', 't6.1']
                  },{
                    kind: 'after-each',
                    title: 'd6',
                    path: [testPath, 'd6']
                  },{
                    kind: 'before-each',
                    title: 'd6',
                    path: [testPath, 'd6']
                  },{
                    kind: 'test',
                    title: 't6.2',
                    path: [testPath, 'd6', 't6.2']
                  },{
                    kind: 'after-each',
                    title: 'd6',
                    path: [testPath, 'd6']
                  }]);

                done();
              }
              catch (e) {
                done(e);
              }
            });
        });
    });
  })
});