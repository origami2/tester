describe('Test', function () {
  var Once = require('../once');
  var assert = require('assert');
  
  it('requires implementation', function () {
    assert.throws(
      function () {
        new Once();
      },
      /implementation is required/
    );
  });
  
  it('accepts implementation', function () {
    new Once(function () {});
  });
  
  
  it('returns function implementation', function () {
    var once = new Once(function () {});
    
    assert(once);
    
    assert(typeof(once), 'function');
  });
  
  it('runs implementation', function (done) {
    var once = new Once(function () {
      done();
    });
    
    once();
  });
  
  it('runs implementation just once', function () {
    var times = 0;
    var once = new Once(function () {
      times++;
    });
    
    once();
    once();
    
    assert.equal(times, 1);
  });
});