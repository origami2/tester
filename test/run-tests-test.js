var RunTests = require('../bin/run-tests');
var assert = require('assert');

describe('run-tests', function () {
  it('exports a function', function () {
    assert.equal(typeof(RunTests), 'function');
  });
  
  it('returns a promise', function () {
    var p = RunTests([]);
    
    assert(p instanceof Promise);
  });
  
  it('resolves promise', function (done) {
    RunTests([require('path').join(__dirname, 'sample-tests')])
    .then(function () {
      done();
    });
  });
  
  it('resolves promise with results', function (done) {
    RunTests([require('path').join(__dirname, 'sample-tests')])
    .then(function (results) {
      try {
        assert(results);
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
});