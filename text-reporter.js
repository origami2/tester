function TextReporter(runner) {
  var path = [];
  
  runner.on('start-run', function (metadata) {
    path.push(metadata);
  });
  runner.on('stop-run', function (metadata) {
    path.pop();
  });
  
  runner.on('run-ok', function (metadata) {
    if (metadata.kind !== 'test') return;
    
    console.log(
      path
      .map(
        function (i) {
          return i.title;
        }
      )
      .join(' => ') +
      ' => ' + metadata.title + ' ran ok');
  });
  runner.on('run-fail', function (metadata) {
    if (metadata.kind !== 'test') return;
    
    console.log(
      path
      .map(
        function (i) {
          return i.title;
        }
      )
      .join(' => ') +
      ' => ' + metadata.title + ' ran failed');
  });
}

module.exports = TextReporter;