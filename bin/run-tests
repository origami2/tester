#! /usr/bin/env node

var scriptMode = !module.parent;
var ObjectReporter = require('../object-reporter');

function run(argv) {
  return new Promise(function (resolve, reject) {
    var paths = argv._;
    var async = require('async');
    var fs = require('fs');
    var TextReporter = require('../text-reporter');
    
    var Runner = require('../runner');
    var MochaLoader = require('../mocha-loader');
    var loader = new MochaLoader();
    
    async
    .concatSeries(
      paths,
      function (path, callback) {
        path = require('path').resolve(path);
        
        async
        .mapSeries(
          fs
          .readdirSync(path)
          .filter(
            function(file){
                // Only keep the .js files
                return file.substr(-3) === '.js';
            }
          ),
          function (file, callback) {
            var filePath = require('path').join(path, file);
            
            try {
              loader
              .loadFile(filePath)
              .then(function (suite) {
                callback(null, suite);
              })
              .catch(function (e) {
              console.log(e, file)
                callback(e);
              });
            } catch (e) {
              callback(e);
            }
          },
          callback
        );
      },
      function (err, suites) {
        if (err) throw err;
        
        var runner = new Runner();
        
        new TextReporter(runner);
        var objectReporter = new ObjectReporter(runner);
        
        async
        .eachSeries(
          suites,
          function (suite, callback) {
            suite
            .run(runner)
            .then(function () {
              callback();
            })
            .catch(callback);
          },
          function (err) {
            if (err && scriptMode) {
              console.error(err);
              process.exit(-1);
            } else if (err) {
              reject(err);
            }
            
            if (!scriptMode) {
              resolve(objectReporter.getResults());
            } else {
              process.exit();
            }
          }
        );
      }
    );
  });
}

if (scriptMode) {
  var argv = require('minimist')(process.argv.slice(2));

  run(argv);
}

module.exports = run;