var shortid = require('shortid');
var Suite = require('./suite');
var Test = require('./test');

function Aggregator(filename) {
  if (!filename) throw new Error('filename is required');
  
  this.tests = [];
  this.rootSuite = new Suite(filename);
  this.path = [this.rootSuite];
}

Aggregator.prototype.getTests = function () {
  return this.tests;
};

Aggregator.prototype.getRootSuite = function () {
  return this.rootSuite;
};

Aggregator.prototype.makeDescribe = function () {
  var self = this;
  
  return function (title, fn) {
    var parent = self.path.slice(-1)[0];
    
    var suite = new Suite(title, parent);
    
    parent.addSuite(suite);
    self.path.push(suite);

    fn();
    
    self.path.splice(-1);
  };
};

Aggregator.prototype.makeContext = function () {
  var self = this;
  
  return function (title, fn) {
    var parent = self.path.slice(-1)[0];
    
    var suite = new Suite(title, parent);
    
    parent.addSuite(suite);
    self.path.push(suite);

    fn();
    
    self.path.splice(-1);
  };
};

Aggregator.prototype.makeBefore = function () {
  var self = this;
  
  return function (fn) {
    var parent = self.path.slice(-1)[0];
    
    parent.addBefore(fn);
  };
};

Aggregator.prototype.makeBeforeEach = function () {
  var self = this;
  
  return function (fn) {
    var parent = self.path.slice(-1)[0];
    
    parent.addBeforeEach(fn);
  };
};

Aggregator.prototype.makeAfterEach = function () {
  var self = this;
  
  return function (fn) {
    var parent = self.path.slice(-1)[0];
    
    parent.addAfterEach(fn);
  };
};

Aggregator.prototype.makeAfter = function () {
  var self = this;
  
  return function (fn) {
    var parent = self.path.slice(-1)[0];
    
    parent.addAfter(fn);
  };
};

Aggregator.prototype.makeIt = function () {
  var self = this;
  
  var it = function (title, fn) {
    var test = new Test(title, fn);
    var parent = self.path.slice(-1)[0];
    
    parent.addTest(test);
    
    self.tests.push(test);
  };
  
  it.skip = function (title, fn) {
    self
    .tests
    .push({
      id: shortid.generate(),
      parent: self.currentPath.length ? self.currentPath.slice(-1)[0].id : 'root',
      title: title,
      path: [].concat(self.currentPath),
      skip: true,
      fn: fn
    });
  };
  
  return it;
};

module.exports = Aggregator;