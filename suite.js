var async = require('async');
var Runnable = require('./runnable');

function Suite(title, parent) {
  if (!title) throw new Error('title is required');

  this.title = title;
  this.tests = [];
  this.suites = [];
  this.path = (parent ? parent.path : []).concat([title]);
  this.hooks = {
    'before': [],
    'beforeEach': [],
    'after': [],
    'afterEach': []
  };
}

Suite.prototype.getSuites = function() {
  return this.suites;
};

Suite.prototype.getTests = function() {
  return this.tests;
};

Suite.prototype.getTitle = function() {
  return this.title;
};

Suite.prototype.addBefore = function(implementation) {
  this.hooks.before.push(new Runnable(implementation));
};

Suite.prototype.addBeforeEach = function(implementation) {
  this.hooks.beforeEach.push(implementation);
};

Suite.prototype.getBeforeEachHooks = function () {
  return this.hooks.beforeEach;
};

Suite.prototype.addAfterEach = function(implementation) {
  this.hooks.afterEach.push(implementation);
};

Suite.prototype.getAfterEachHooks = function () {
  return this.hooks.afterEach;
};

Suite.prototype.addAfter = function(implementation) {
  this.hooks.after.push(implementation);
};

Suite.prototype.getBeforeHooks = function() {
  return this.hooks.before;
};

Suite.prototype.getAfterHooks = function() {
  return this.hooks.after;
};

Suite.prototype.addTest = function(test) {
  if (!test) throw new Error('test is required');

  this.tests.push(test);
};

Suite.prototype.addSuite = function(suite) {
  if (!suite) throw new Error('suite is required');

  if (this.suites.indexOf(suite) !== -1) return;
  this.suites.push(suite);
};

Suite.prototype.run = function(runner) {
  if (!runner) throw new Error('runner is required');

  var self = this;

  return new Promise(function(resolve, reject) {
    var toRun = [];
    
    for (var i = 0; i < self.hooks.before.length; i++) {
      var h = self.hooks.before[i];
      
      toRun.push({
        path: [].concat(self.path),
        title: self.getTitle(),
        kind: 'before',
        runnable: h
      });
    }    
    
    for (var i = 0; i < self.suites.length; i++) {
      for (var j = 0; j < self.hooks.beforeEach.length; j++) {
        var h = self.hooks.beforeEach[j];
      
        toRun.push({
          path: [].concat(self.path),
          title: self.getTitle(),
          kind: 'before-each',
          runnable: new Runnable(h)
        });
      }
      
      var s = self.suites[i];
      
      toRun.push({
        path: [].concat(self.path).concat([s.getTitle()]),
        title: s.getTitle(),
        kind: 'suite',
        runnable: s
      });
      
      for (var j = 0; j < self.hooks.afterEach.length; j++) {
        var h = self.hooks.afterEach[j];
      
        toRun.push({
          path: [].concat(self.path),
          title: self.getTitle(),
          kind: 'after-each',
          runnable: new Runnable(h)
        });
      }
    }
    
    for (var i = 0; i < self.tests.length; i++) {
      for (var j = 0; j < self.hooks.beforeEach.length; j++) {
        var h = self.hooks.beforeEach[j];
      
        toRun.push({
          path: [].concat(self.path),
          title: self.getTitle(),
          kind: 'before-each',
          runnable: new Runnable(h)
        });
      }
      
      var t = self.tests[i];
      
      toRun.push({
        path: [].concat(self.path).concat([t.getTitle()]),
        title: t.getTitle(),
        kind: 'test',
        runnable: t
      });
      
      for (var j = 0; j < self.hooks.afterEach.length; j++) {
        var h = self.hooks.afterEach[j];
      
        toRun.push({
          path: [].concat(self.path),
          title: self.getTitle(),
          kind: 'after-each',
          runnable: new Runnable(h)
        });
      }
    }
    
    for (var i = 0; i < self.hooks.after.length; i++) {
      var h = self.hooks.after[i];
      
      toRun.push({
        path: [].concat(self.path),
        title: self.getTitle(),
        kind: 'after',
        runnable: new Runnable(h)
      });
    }
    
    async
    .eachSeries(
      toRun,
      function(item, callback) {
        runner
          .run({
            path: item.path,
            kind: item.kind,
            title: item.title
          }, item.runnable)
          .catch(function() {
            callback();
          })
          .then(function() {
            callback();
          });
      },
      function(err) {
        if (err) return reject(err);

        resolve();
      }
    );
  });
};

Suite.prototype.getAllTests = function () {
  var all = [];
  
  for (var i = 0; i < this.suites.length; i++) {
    all = all.concat(this.suites[i].getAllTests());
  }
  
  all = all.concat(this.getTests());
  
  return all;
};

module.exports = Suite;