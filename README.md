# Origami stress

### Purpose

To reuse mocha test for load/stress testing

- [x] Sequential testing
- [ ] Parallel testing
- [ ] Stress testing

### Installation

`npm install -g origami-stress`

### Usage

`stress {options} [testPath1 ... testPathX]`

options can be:
`-j output.json`: saves the results into output.json. Read about [its format](#results-format)
`-e`: do not output errors to console
`-s`: be silent
`-t`: set the timeout to run each test, in milliseconds. 5000 milliseconds by default
`-v`: show version and exit

### <a id="results-format">Results format</a>

```json
{
  "timeout": 5000,
  "tests": {
    "parent1": {
      "children1": {
        "grandchildren1": {
          "errors": 0,
          "times": 0
        }
      }
    }
  }
}
```

Leaf nodes under `tests` will contain `errors` and `times` properties.
`timeout` property is specified in milliseconds.