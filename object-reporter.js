function ObjectReporter(runner) {
  if (!runner) throw new Error('runner is required');
  var results = this.results = {};
  
  runner
  .on(
    'start-run',
    function (metadata) {
      results[metadata.title] = {
        start: metadata.date,
        kind: metadata.kind
      };
    }
  );
  
  runner
  .on(
    'stop-run',
    function (metadata) {
      results[metadata.title].end = metadata.date;
      results[metadata.title].took = results[metadata.title].end - results[metadata.title].start;
    }
  );
}

ObjectReporter.prototype.getResults = function () {
  return this.results;
};

module.exports = ObjectReporter;