function Runnable(implementation) {
  if (!implementation) throw new Error('implementation is required');
  
  this.implementation = implementation;
}

Runnable.prototype.run = function () {
  var self = this;
  
  return new Promise(function (resolve, reject) {
    if (self.implementation.length === 1) {
      try {
        self
        .implementation(function (err) {
          if (err) return reject(err);
          
          resolve();
        });
      } catch (e) {
        if (e instanceof Error) {
          reject({
            stack: e.stack,
            message: e.message,
            name: e.name
          });
        } else reject(e);
      }
    } else {
      try {
        self.implementation();
        
        resolve();
      } catch (e) {
        if (e instanceof Error) {
          reject(e);
        } else reject(e);
      }
    }
  });
};

module.exports = Runnable;