function Once(implementation) {
  if (!implementation) throw new Error('implementation is required');
  
  var ran = false;
  
  return function () {
    if (ran) return;
    ran = true;
    
    var args = [];
    
    for (var i = 0; i < arguments.length; i++) {
      args.push(arguments[i]);
    }
      
    return implementation.apply(this, args);
  };
}

module.exports = Once;