var async = require('async');
var Runnable = require('./runnable');

function Test(title, implementation) {
  if (!title) throw new Error('title is required');
  if (!implementation) throw new Error('implementation is required');

  this.title = title;
  this.implementation = implementation;
  this.preRun = [];
  this.postRun = [];
}

Test.prototype.getTitle = function () {
  return this.title;
};

Test.prototype.addPreRun = function(runnable) {
  if (!runnable) throw new Error('runnable is required');

  this.preRun.push(runnable);
};

Test.prototype.addPostRun = function(runnable) {
  if (!runnable) throw new Error('runnable is required');

  this.postRun.push(runnable);
};

Test.prototype.run = function() {
  var self = this;

  return new Promise(function(resolve, reject) {
    async
    .eachSeries(
      [].concat(
        self.preRun, 
        [new Runnable(self.implementation)], 
        self.postRun
      ),
      function(item, callback) {
        item.run().then(function() {
            callback();
          })
          .catch(function(err) {
            callback(err);
          })
      },
      function (err) {
        if (err) return reject(err);
        
        resolve();
      }
    )
  });
};

module.exports = Test;